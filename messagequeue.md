# Message Queue

The concept of a messaging queue is primarily to allow easy communication between applications. The message queue helps in this by providing temporary message storage when the destination program is busy or not connected.

To understand it more deeply let us understand what is a queue and a message separately.

## Queue

A queue is a line of things waiting to be addressed or handled, this usually follows a sequential order i.e. starting from the beginning of the line and processing it till the end of the line. So, a message queue is the queue of messages sent between applications. It includes a sequence of work objects that are waiting to be processed.

## Message

A message is the data transported between the sender and the receiver application. It may be a byte array with some headers at the top. A message could be something that could contain information about the start of a task, or maybe finishing of a task or it can be a simple plain text message.

<p align="center">
  <img width="460" height="300" src="https://miro.medium.com/max/700/1*cCyPNzf95ygMFUgsrleHtw.png ">
</p>

The **basic architecture** of the message queue is simple; there are client applications called producers that create messages and deliver them to the message queue. Another application, called a consumer, connects to the queue and gets the messages to be processed. Messages placed onto the queue are stored until the consumer retrieves them.

## Benefits of Message Queues

In modern architecture, applications are decoupled into smaller, independent applications that are easier to develop and maintain and deploy. Message queues provide communication and coordination from these distributed small-small applications.

* ### Better Performance

    Message Queue enables asynchronous communication, which means that the endpoints that are producing and consuming messages interact with the queue, not each other. Producers can add requests to the queue without waiting for them to be processed.

* ### Granular Scalability

    Message queues make it possible to scale precisely where you need to. When workloads peak, multiple instances of your application can all add requests to the queue without risk of collision. As your queues get longer with these incoming requests, you can distribute the workload across a fleet of consumers. Producers, consumers, and the queue itself can all grow and shrink on demand.

* ### Increased Reliability

    Queues make your data persistent and reduce the errors that happen when different parts of your system go offline. By separating different components with message queues, you create more fault tolerance. If one part of the system is ever unreachable, the other can still continue to interact with the queue. The queue itself can also be mirrored for even more availability.

### Message queuing - a simple use case

Imagine that you have a web service that has many requests coming each second, and it is important that no request should be lost, and all request needs to be processed by a function that has a high throughput. This means that the web service should be highly available and ready to receive a new request instead of being locked by the processing of the previous request. In this case, placing a queue between the web service and processing service is ideal.

## What are the best Message Queue Tools?

Kafka, RabbitMQ, Amazon SQS, and ActiveMQ are the most popular tools in the **Message Queue** world. High-throughput is the primary reason developers pick Kafka over its competitors, while  RabbitMQ is chosen as it's fast and has good metrics/monitoring.

We will cover the basics of Apache Kafka and RabbitMQ in this article.

## APACHE KAFKA

Apache Kafka is an open-source distributed event streaming platform used by thousands of companies for high-performance data pipelines, streaming analytics, data integration, and mission-critical applications.

### Core Capabilities- Apache Kafka

* High Throughput
* Scalable
* Permanent Storage
* High Availability
* Connect to almost anything

### Example of Kafka in SpringBoot

* #### Configuration-

```yaml
server: port: 9000
spring:
   kafka:
     consumer:
        bootstrap-servers: localhost:9092
        group-id: group_id
        auto-offset-reset: earliest
        key-deserializer: org.apache.kafka.common.serialization.StringDeserializer
        value-deserializer: org.apache.kafka.common.serialization.StringDeserializer
     producer:
        bootstrap-servers: localhost:9092
        key-serializer: org.apache.kafka.common.serialization.StringSerializer
        value-serializer: org.apache.kafka.common.serialization.StringSerializer
```

* #### Creating Producer

```java
@Service
public class Producer {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);
    private static final String TOPIC = "users";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(String message) {
        logger.info(String.format("#### -> Producing message -> %s", message));
        this.kafkaTemplate.send(TOPIC, message);
    }
}
```

* #### Creating Consumer

```java
@Service
public class Consumer {

    private final Logger logger = LoggerFactory.getLogger(Producer.class);

    @KafkaListener(topics = "users", groupId = "group_id")
    public void consume(String message) throws IOException {
        logger.info(String.format("#### -> Consumed message -> %s", message));
    }
}
```

* #### Sending Message to Kafka

```bash
curl -X POST -F 'message=test' http://localhost:9000/kafka/publish
```

## RabitMQ, What is that?

RabbitMQ is an open-source message broker and it is also the most popular message broker with tens of thousands of users. RabbitMQ is lightweight and easy to deploy on the cloud and on-premises. It supports multiple messaging protocols. It can run on many operating systems and cloud environments and provide a wide range of developer tools.

<p align="center">
  <img width="460" height="300" src="https://www.cloudamqp.com/img/blog/exchanges-topic-fanout-direct.png">
</p>

### Benefits of RabbitMQ

* Flexibility
* Highly Available Queue
* Multi-Protocol
* Many Client
* Clustering
* Management UI
* Tracing (Using dashboard can trace support)

## What is an EMB?

An **Enterprise message bus** also referred to as Enterprise Service Bus, is an architectural pattern where a software component acts in a centralized manner and performs integrations between applications. It performs transformations of data models, handles connectivity, performs message routing, covers communication protocols, and manages the composition of multiple requests.
<p align="center">
  <img width="690" height="450" src="https://www.cetrixcloudservices.com/hs-fs/hubfs/Fig%201%2C%20Enterprise%20Service%20Bus.png?width=600&name=Fig%201%2C%20Enterprise%20Service%20Bus.png">
</p>
The ESM pattern is typically implemented using a specially designed integration runtime and toolset that ensures the best possible productivity.

## References

1. [AWS Messaging Queue](https://aws.amazon.com/message-queue/)
2. [Message Queuing](https://www.cloudamqp.com/blog/what-is-message-queuing.html) (2019-09-25)
3. [CloudAMQP](https://www.cloudamqp.com/docs/index.html)
4. [RabbitMQ](https://www.rabbitmq.com/)
5. [RabbitMQ- medium.com](https://medium.com/@kamilmasyhur/rabbitmq-what-is-that-10c74ac7620a)
6. [Apache Kafka and Spring Boot](https://www.confluent.io/blog/apache-kafka-spring-boot-application/)
7. [Apache Kafka](https://kafka.apache.org/)
8. [Enterprise Service Bus](https://en.wikipedia.org/wiki/Enterprise_service_bus)(29 March 2021)
9. [IBM ESB](https://www.ibm.com/cloud/learn/esb)(By: IBM Cloud Education,7 April 2021)
